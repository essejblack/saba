<?php
require_once base_path . './vendor/autoload.php';
//require_once base_path . './config/db.php';
require_once base_path . './config/packages.php';


use Symfony\Component\Dotenv\Dotenv;
(new Dotenv())->usePutenv(true)->bootEnv(base_path.'/.env');
