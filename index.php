<?php

const base_path = __DIR__;
require_once base_path . './bootstrap/app.php';


$connection = new \PDO("sqlite:" . getenv('PATH_TO_SQLITE_FILE'));


$shorty = new ShortTheLink(getenv('APP_URL'), $connection);
$shorty->set_chars(getenv('SHORTENER_CHARS'));
$shorty->set_salt(getenv('SHORTENER_SALT'));
$shorty->set_padding(getenv('SHORTENER_PADDING'));
$shorty->run();
