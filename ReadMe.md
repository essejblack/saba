By : Mohammad Ehsan Jahanara

Project : Link(URL) Shortener Mini Project

Start : 11:00am Friday

End : 3:25pm Friday

```
APP_URL = http://localhost/saba
```

if you need , you can change it

*for test*

you should set or send parameter as url it will return url
Get Or Post

as following :

http://localhost/saba/?url=http://localhost/saba/shorted-link-isset

it returns : http://localhost/saba/bFpMUl7

if you open http://localhost/saba/bFpMUl7
it will redirect to http://localhost/saba/shorted-link-isset

POSTMAN Collection is in root folder : 

saba.postman_collection.json

