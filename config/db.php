<?php

use App\SQLiteConnection;

$connectionType = env('DB_CONNECTION');
switch ($connectionType) {
    case 'sqlite':
    default:
        $pdo = (new SQLiteConnection())->connect();
        if ($pdo != null)
            echo 'Connected to the SQLite database successfully!';
        else
            echo 'Whoops, could not connect to the SQLite database!';
        break;
}