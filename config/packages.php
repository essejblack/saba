<?php

$PackagePath = base_path . '/packages/';


$packages = [
    'shortener'
];

foreach ($packages as $package) {
    require_once $PackagePath . $package . '/index.php';
}